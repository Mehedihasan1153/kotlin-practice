• Android architecture component (Room, Live data and viewModel) is used to design the application with MVVM architecture pattern which gives the flexibility for unit testing.

• When the app is opened it checks with the internet to download the employee data. The data is saved in the database so if there are no internet connection
it can still load the data from the database.

• For the network operation Volley library is used.

• recyclerview is used to show the data in the recycler view.

• Material design is used to show the edit text view. 

• db folder: contains all the class with database operation. The DAO interface defines all the database operations to do on entity. 
Since Room doesn't allow database queries on the main thread, AsyncTasks is used to execute them asynchronously.

• adapter Folder: adapter class is used for loading the employee data into recycler view item

• viewModel folder:The ViewModel class allows data to survive configuration changes such as screen rotations. 
In the viewModel class Live data is used so the updated data is observed immediately by the observers and the user data in the recycler view is updated immediately