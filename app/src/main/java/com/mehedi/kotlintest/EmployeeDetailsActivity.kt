package com.mehedi.kotlintest

import android.arch.lifecycle.ViewModelProviders
import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.util.Log
import android.view.Menu
import android.view.MenuItem
import com.mehedi.kotlintest.db.Employee
import com.mehedi.kotlintest.db.EmployeeDao
import com.mehedi.kotlintest.db.EmployeeDatabase
import com.mehedi.kotlintest.viewModel.EmployeeListViewModel
import kotlinx.android.synthetic.main.activity_employee_details.*

class EmployeeDetailsActivity : AppCompatActivity() {

    private val TAG = "HOOK_AND_LOOP_TEST"
    private var employeeDao: EmployeeDao? = null
    private var viewModel: EmployeeListViewModel? = null

    private var currentEmployee: String? = null
    private var employee: Employee? = null


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_employee_details)

        var db: EmployeeDatabase = EmployeeDatabase.getDataBase(this)

        employeeDao = db.employeeDao()

        viewModel = ViewModelProviders.of(this).get(EmployeeListViewModel::class.java)
        currentEmployee = intent.getStringExtra("idEmployee")
        Log.d(TAG, "Current Employee ID is: " + currentEmployee)

        setTitle("Employee Details")
        employee = employeeDao!!.getEmployeeById(currentEmployee!!)
        name_text_view!!.setText("Employee Name: " + employee!!.name)
        age_text_view!!.setText("Employee Age: " + employee!!.age)
        salary_text_view!!.setText("Employee Salary: " + employee!!.salary)

    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.menu_edit, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            R.id.edit_item -> {
                //clicking on the edit button will launch new activity with edit option
                var intent = Intent(applicationContext, EditEmployeeActivity::class.java)
                intent.putExtra("idEmployee", employee?.id)
                startActivity(intent)
            }
        }
        return super.onOptionsItemSelected(item)
    }

    override fun onStop() {
        super.onStop()
        finish()
    }
}
