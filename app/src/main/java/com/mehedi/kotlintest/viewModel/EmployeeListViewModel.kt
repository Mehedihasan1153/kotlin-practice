package com.mehedi.kotlintest.viewModel

import android.app.Application
import android.arch.lifecycle.AndroidViewModel
import android.arch.lifecycle.LiveData
import android.os.AsyncTask
import com.mehedi.kotlintest.db.Employee
import com.mehedi.kotlintest.db.EmployeeDatabase


class EmployeeListViewModel(application: Application) : AndroidViewModel(application) {

    var listEmployee: LiveData<List<Employee>>
    private val appDb: EmployeeDatabase

    init {
        appDb = EmployeeDatabase.getDataBase(this.getApplication())
        listEmployee = appDb.employeeDao().getAllEmployees()
    }

    fun getListEmployees(): LiveData<List<Employee>> {
        return listEmployee
    }

    fun addEmployee(employee: Employee) {
        addAsynTask(appDb).execute(employee)
    }


    class addAsynTask(db: EmployeeDatabase) : AsyncTask<Employee, Void, Void>() {
        private var EmployeeDatabase = db
        override fun doInBackground(vararg params: Employee): Void? {
            EmployeeDatabase.employeeDao().insertEmployee(params[0])
            return null
        }

    }

}