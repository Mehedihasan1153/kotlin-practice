package com.mehedi.kotlintest.db

import android.arch.persistence.room.Database
import android.arch.persistence.room.Room
import android.arch.persistence.room.RoomDatabase
import android.content.Context

@Database(entities = [(Employee::class)], version = 1, exportSchema = false)
abstract class EmployeeDatabase : RoomDatabase() {
    companion object {
        private var INSTANCE: EmployeeDatabase? = null
        fun getDataBase(context: Context): EmployeeDatabase {
            if (INSTANCE == null) {
                INSTANCE = Room.databaseBuilder(context.applicationContext, EmployeeDatabase::class.java, "employees-db")
                    .allowMainThreadQueries().build()
            }
            return INSTANCE as EmployeeDatabase
        }
    }

    abstract fun employeeDao(): EmployeeDao
}