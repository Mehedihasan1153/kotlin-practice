package com.mehedi.kotlintest.db

import android.arch.lifecycle.LiveData
import android.arch.persistence.room.*


@Dao
interface EmployeeDao {
    @Query("select * from employee")
    fun getAllEmployees(): LiveData<List<Employee>>

    @Query("select * from employee where id in (:id)")
    fun getEmployeeById(id: String): Employee

    @Query("delete from employee")
    fun deleteAllEmployees()

    @Insert(onConflict = OnConflictStrategy.IGNORE)
    fun insertEmployee(employee: Employee)


    @Update
    fun updateEmployee(employee: Employee)

    @Delete
    fun deleteEmployee(employee: Employee)
}