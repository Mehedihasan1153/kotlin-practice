package com.mehedi.kotlintest.db;

import android.arch.persistence.room.ColumnInfo
import android.arch.persistence.room.Entity
import android.arch.persistence.room.PrimaryKey

@Entity(tableName = "employee")
data class Employee(
    @PrimaryKey(autoGenerate = false)
    @ColumnInfo(name = "id")
    var id: String = "",

    @ColumnInfo(name = "name")
    var name: String = "",

    @ColumnInfo(name = "age")
    var age: String = "",

    @ColumnInfo(name = "salary")
    var salary: String = "",

    @ColumnInfo(name = "picture")
    var picture: String = ""

)