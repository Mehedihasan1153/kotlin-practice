package com.mehedi.kotlintest.adapter

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import com.mehedi.kotlintest.R
import com.mehedi.kotlintest.db.Employee


class EmployeeRecyclerAdapter(employees: ArrayList<Employee>, listener: OnItemClickListener) :
    RecyclerView.Adapter<EmployeeRecyclerAdapter.RecyclerViewHolder>() {

    private var listEmployees: List<Employee> = employees

    private var listenerEmployee: OnItemClickListener = listener

    interface OnItemClickListener {
        fun onItemClick(employee: Employee)
    }

    override fun onCreateViewHolder(parent: ViewGroup?, viewType: Int): RecyclerViewHolder {
        return RecyclerViewHolder(LayoutInflater.from(parent!!.context).inflate(R.layout.item_list, parent, false))
    }

    override fun getItemCount(): Int {
        return listEmployees.size
    }

    override fun onBindViewHolder(holder: RecyclerViewHolder?, position: Int) {
        var currentEmployee: Employee = listEmployees[position]

        var nameEmployee = currentEmployee.name


        holder!!.mName.text = nameEmployee


        holder.bind(currentEmployee, listenerEmployee)

    }

    fun addEmployees(listEmployees: List<Employee>) {
        this.listEmployees = listEmployees
        notifyDataSetChanged()
    }


    class RecyclerViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        var mName = itemView.findViewById<TextView>(R.id.text_view_employee_name)!!

        fun bind(employee: Employee, listener: OnItemClickListener) {
            itemView.setOnClickListener {
                listener.onItemClick(employee)
            }
        }

    }
}