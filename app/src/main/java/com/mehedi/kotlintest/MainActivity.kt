package com.mehedi.kotlintest

import android.arch.lifecycle.Observer
import android.arch.lifecycle.ViewModelProviders
import android.content.Context
import android.content.Intent
import android.net.ConnectivityManager
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.util.Log
import android.view.Gravity
import android.widget.Toast
import com.android.volley.Request
import com.android.volley.Response
import com.android.volley.toolbox.JsonArrayRequest
import com.android.volley.toolbox.Volley
import com.mehedi.kotlintest.adapter.EmployeeRecyclerAdapter
import com.mehedi.kotlintest.db.Employee
import com.mehedi.kotlintest.db.EmployeeDatabase
import com.mehedi.kotlintest.viewModel.EmployeeListViewModel
import kotlinx.android.synthetic.main.activity_main.*
import org.json.JSONArray
import org.json.JSONObject
import java.util.*

class MainActivity : AppCompatActivity(), EmployeeRecyclerAdapter.OnItemClickListener {

    internal var employeeList: MutableList<Employee>? = null
    private var employeeRecyclerView: RecyclerView? = null
    private var recyclerViewAdapter: EmployeeRecyclerAdapter? = null
    private val TAG = "HOOK_AND_LOOP_TEST"

    private var viewModel: EmployeeListViewModel? = null

    private var db: EmployeeDatabase? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        //check if internet connection is available before starting loading data from server
        if (isNetworkAvailable()) {
            loadEmployeeData()
        } else {
            Log.d(TAG, "device is not connected to internet")
            val toast = Toast.makeText(this, "PLEASE CONNECT TO INTERNET TO GET DATA FROM SERVER", Toast.LENGTH_LONG)
            toast.setGravity(Gravity.CENTER, 0, 0)
            toast.show()
        }

        db = EmployeeDatabase.getDataBase(this)

        employeeRecyclerView = findViewById(R.id.recycler_view)
        recyclerViewAdapter = EmployeeRecyclerAdapter(arrayListOf(), this)

        employeeRecyclerView!!.layoutManager = LinearLayoutManager(this)
        employeeRecyclerView!!.adapter = recyclerViewAdapter

        //pass the correct viewModel instance where the Activity is passed to this
        // ViewModel's lifecycle should be scoped to.
        viewModel = ViewModelProviders.of(this).get(EmployeeListViewModel::class.java)

        //observing the live data. if the data changed it will be notified immmediately
        viewModel!!.getListEmployees().observe(this, Observer { employees ->
            recyclerViewAdapter!!.addEmployees(employees!!)
        })

        //floating action button to add new employee information.
        //It will launch a new activity to add user data
        fab.setOnClickListener {
            var intent = Intent(applicationContext, EditEmployeeActivity::class.java)
            startActivity(intent)
        }
    }

    private fun isNetworkAvailable(): Boolean {
        val connectivityManager = getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
        val activeNetworkInfo = connectivityManager.activeNetworkInfo
        return activeNetworkInfo != null && activeNetworkInfo.isConnected
    }

    //insert the data from the internet into the database.
    //this data can be loaded later if network is not available
    fun insertEmployeeData(employees: MutableList<Employee>) {

        for (employee in employees) {
            viewModel?.addEmployee(employee)
        }
    }

    fun loadEmployeeData() {

        //using volley library to load the data from the server
        val queue = Volley.newRequestQueue(this)
        val url = "http://dummy.restapiexample.com/api/v1/employees"
        employeeList = ArrayList<Employee>()

        val request = JsonArrayRequest(
            Request.Method.GET, url, null,
            Response.Listener<JSONArray> { response ->

                for (i in 0..response!!.length() - 1) {
                    val jsonObject: JSONObject = response.getJSONObject(i);

                    val id = jsonObject.getString("id")
                    val name = jsonObject.getString("employee_name")
                    val salary = jsonObject.getString("employee_salary")
                    val age = jsonObject.getString("employee_age")
                    val picture = jsonObject.getString("profile_image")

                    val employee = Employee(id, name, age, salary, picture)
                    employeeList?.add(employee)
                }

                insertEmployeeData(this!!.employeeList!!)

            },

            Response.ErrorListener {
                Log.d(TAG, "Json data fetch" + it.message)
            })

        queue.add(request)

    }

    //when each item is clicked, it will show the details information of that employee in another activity
    override fun onItemClick(employee: Employee) {
        var intent = Intent(applicationContext, EmployeeDetailsActivity::class.java)
        intent.putExtra("idEmployee", employee.id)
        startActivity(intent)
    }
}


