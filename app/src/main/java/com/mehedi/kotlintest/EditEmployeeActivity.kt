package com.mehedi.kotlintest

import android.arch.lifecycle.ViewModelProviders
import android.content.Context
import android.net.ConnectivityManager
import android.os.AsyncTask
import android.os.Bundle
import android.support.v7.app.AlertDialog
import android.support.v7.app.AppCompatActivity
import android.util.Log
import android.view.Gravity
import android.view.Menu
import android.view.MenuInflater
import android.view.MenuItem
import android.widget.Toast
import com.mehedi.kotlintest.db.Employee
import com.mehedi.kotlintest.db.EmployeeDao
import com.mehedi.kotlintest.db.EmployeeDatabase
import com.mehedi.kotlintest.viewModel.EmployeeListViewModel
import kotlinx.android.synthetic.main.activity_edit_employee.*
import org.json.JSONObject
import java.io.*
import java.net.HttpURLConnection
import java.net.URL


class EditEmployeeActivity : AppCompatActivity() {

    private val TAG = "HOOK_AND_LOOP_TEST"
    private var employeeDao: EmployeeDao? = null
    private var viewModel: EmployeeListViewModel? = null

    private var currentEmployee: String? = null
    private var employee: Employee? = null


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_edit_employee)

        var db: EmployeeDatabase = EmployeeDatabase.getDataBase(this)

        employeeDao = db.employeeDao()

        viewModel = ViewModelProviders.of(this).get(EmployeeListViewModel::class.java)
        currentEmployee = intent.getStringExtra("idEmployee")


        if (currentEmployee != null) {
            //this means this is edit activity
            setTitle(R.string.edit_employee_title)
            employee = employeeDao!!.getEmployeeById((currentEmployee!!))

            edit_text_name.setText(employee!!.name)
            edit_text_salary.setText(employee!!.salary)
            edit_text_age.setText(employee!!.age)

        } else {
            setTitle(R.string.add_employee_title)
            invalidateOptionsMenu()
        }
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        var inflater: MenuInflater = menuInflater
        inflater.inflate(R.menu.menu_items, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        when (item!!.itemId) {
            R.id.done_item -> {
                if (currentEmployee == null) {
                    saveEmployee()

                } else {
                    updateEmployee()
                    Toast.makeText(this, getString(R.string.update_employee), Toast.LENGTH_SHORT).show()
                    finish()
                }

            }

        }
        return super.onOptionsItemSelected(item)
    }


    private fun saveEmployee() {
        if (isNetworkAvailable()) {

            var nameEmployee = edit_text_name!!.text.toString()
            var salaryEmployee = edit_text_salary!!.text.toString()
            var ageEmployee = edit_text_age!!.text.toString()

            val json = JSONObject()
            json.put("name", nameEmployee)
            json.put("salary", salaryEmployee)
            json.put("age", ageEmployee)

            Log.d(TAG, "Starting the network operation")

            MyAsyncTask().execute(json.toString())

        } else {
            Log.d(TAG, "device is not connected to internet")
            val toast = Toast.makeText(this, "PLEASE CONNECT TO INTERNET TO ADD DATA TO SERVER", Toast.LENGTH_LONG)
            toast.setGravity(Gravity.CENTER, 0, 0)
            toast.show()
        }

    }

    private fun updateEmployee() {
        var nameEmployee = edit_text_name!!.text.toString()
        var salaryEmployee = edit_text_salary!!.text.toString()
        var ageEmployee = edit_text_age!!.text.toString()

        var employee = Employee(employee!!.id, nameEmployee, ageEmployee, salaryEmployee, "")
        employeeDao!!.updateEmployee(employee)
    }

    //after successfully created the employee data in the server this method would call to show alert dialog
    private fun showAlertDialog(id: String) {

        var nameEmployee = edit_text_name!!.text.toString()
        var salaryEmployee = edit_text_salary!!.text.toString()
        var ageEmployee = edit_text_age!!.text.toString()

        var employee = Employee(id, nameEmployee, ageEmployee, salaryEmployee, "")
        viewModel!!.addEmployee(employee)

        //creating the alertdialog
        val builder = AlertDialog.Builder(this)
        builder.setTitle("Employee Addition")
        builder.setMessage("New Employee information is successfully added to server")

        builder.setPositiveButton(android.R.string.yes) { dialog, which ->
            finish()
        }

        builder.show()

    }

    private fun isNetworkAvailable(): Boolean {
        val connectivityManager = getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
        val activeNetworkInfo = connectivityManager.activeNetworkInfo
        return activeNetworkInfo != null && activeNetworkInfo.isConnected
    }


    inner class MyAsyncTask : AsyncTask<String, Void, String>() {

        override fun doInBackground(vararg params: String): String? {

            var id: String? = null
            val url = URL("http://dummy.restapiexample.com/api/v1/create")
            val httpClient = url.openConnection() as HttpURLConnection
            httpClient.setReadTimeout(10000)
            httpClient.setConnectTimeout(10000)
            httpClient.requestMethod = "POST"
            httpClient.instanceFollowRedirects = false
            httpClient.doOutput = true
            httpClient.doInput = true
            httpClient.useCaches = false
            httpClient.setRequestProperty("Content-Type", "application/json; charset=utf-8")

            try {

                httpClient.connect()
                val os = httpClient.getOutputStream()
                val writer = BufferedWriter(OutputStreamWriter(os, "UTF-8"))
                writer.write(params[0])
                writer.flush()
                writer.close()
                os.close()

                if (httpClient.responseCode == HttpURLConnection.HTTP_OK) {
                    val stream = BufferedInputStream(httpClient.inputStream)

                    val bufferedReader = BufferedReader(InputStreamReader(stream))

                    val json = bufferedReader.readLine()

                    val jsonObject = JSONObject(json)

                    id = jsonObject.getString("id")

                    Log.d(TAG, "getng the id" + id)


                } else {
                    println("ERROR ${httpClient.responseCode}")
                }
            } catch (e: Exception) {
                e.printStackTrace()

            } finally {
                httpClient.disconnect()
            }

            return id

        }


        override fun onPostExecute(result: String) {
            super.onPostExecute(result)
            showAlertDialog(result)

        }
    }


}
